use num::complex::Complex;
use rand::Rng;
use rayon::prelude::{IntoParallelIterator, ParallelIterator};

const DEFAULT_IMG_X: u32 = 700;
const DEFAULT_IMG_Y: u32 = 700;
const MAX_ITERATIONS: u16 = 256;

struct FractalCore;
impl FractalCore {
    pub fn get_cords(width: u32, position: usize) -> (u32, u32) {
        let p = position + 1;
        (
            (p as f64 / width as f64) as u32,
            (p % width as usize) as u32 + width,
        )
    }
    //Rust > 1.31 #[allow(clippy::many_single_char_names)]
    pub fn math_closure(width: u32, height: u32) -> impl Fn(usize) -> u8 {
        let mut rng = rand::thread_rng();
        let zoom = rng.gen_range(1f32..10f32) / rng.gen_range(20f32..50f32);

        let scalex = 4f32 / width as f32;
        let scaley = 4f32 / height as f32;

        move |pixel| {
            let cords = Self::get_cords(width, pixel);
            let y: u32 = cords.0;
            let x: u32 = cords.1;
            let cy = (y as f32 * scaley - 2.0) * zoom;
            let cx = (x as f32 * scalex - 2.0) * zoom;
            let mut z = Complex::new(cx, cy);
            let c = Complex::new(-0.4, 0.6);
            let mut i = 0;
            for t in 0..MAX_ITERATIONS {
                if (z.norm() > 2.0) || (i > 800) {
                    break;
                }
                z = z * z + c;
                i = t;
            }
            if i < 800 {
                return i as u8;
            };
            0u8
        }
    }

    #[allow(unused)]
    pub fn sync(width: u32, height: u32) -> FractalSync {
        FractalSync::new(width, height)
    }
    #[allow(unused)]
    pub fn parallel(width: u32, height: u32) -> Vec<u8> {
        let upper = width as usize * height as usize;
        (0..upper)
            .into_par_iter()
            .map(FractalCore::math_closure(width, height))
            .collect()
    }
}

pub struct FractalSync {
    width: u32,
    height: u32,
    pos: usize,
    end: usize,
    closure: Box<dyn Fn(usize) -> u8>,
}

impl FractalSync {
    #[allow(unused)]
    fn new(width: u32, height: u32) -> Self {
        FractalSync {
            width,
            height,
            pos: 0,
            end: (width as usize * height as usize),
            closure: Box::new(FractalCore::math_closure(width, height)),
        }
    }
    #[allow(unused)]
    const fn dimentions(&self) -> (u32, u32) {
        (self.width, self.height)
    }
}

impl std::io::Read for FractalSync {
    fn read(&mut self, buf: &mut [u8]) -> std::io::Result<usize> {
        let mut i = 0;
        for b in buf {
            match self.next() {
                Some(byte) => {
                    *b = byte;
                    i += 1;
                }
                None => break,
            }
        }
        Ok(i)
    }
}

impl Iterator for FractalSync {
    type Item = u8;
    fn next(&mut self) -> Option<Self::Item> {
        if self.pos == self.end {
            return None;
        }
        self.pos += 1;
        Some((self.closure)(self.pos))
    }
}

pub async fn fractal_png() -> actix_web::Result<::actix_web::HttpResponse> {
    let imgx: u32 = match std::env::var("FRACTAL_RES_X") {
        Ok(val) => match val.parse() {
            Ok(x) => x,
            Err(_) => DEFAULT_IMG_X,
        },
        Err(_) => DEFAULT_IMG_X,
    };
    let imgy: u32 = match std::env::var("FRACTAL_RES_Y") {
        Ok(val) => match val.parse() {
            Ok(x) => x,
            Err(_) => DEFAULT_IMG_Y,
        },
        Err(_) => DEFAULT_IMG_Y,
    };
    let counter = std::sync::atomic::AtomicUsize::new(0);
    let order = std::sync::atomic::Ordering::Acquire;
    let math = FractalCore::math_closure(imgx, imgy);

    let imgbuf = ::image::ImageBuffer::from_fn(imgx, imgy, |_, _| {
        let pos = counter.fetch_add(1, order);
        let pix = math(pos);
        image::Luma([pix])
    });
    // Return the image without saving to disk
    let mut png_buf = Vec::with_capacity(imgbuf.len());

    image::DynamicImage::ImageLuma8(imgbuf)
        .write_to(&mut png_buf, image::ImageOutputFormat::Png)
        .map_err(|_| std::io::Error::new(std::io::ErrorKind::Other, "Failed to process image"))?;
    Ok(::actix_web::HttpResponse::Ok()
        .content_type("image/png")
        .body(png_buf))
}

#[test]
fn sync_iter() {
    assert_eq!(FractalCore::sync(700, 700).count(), 490_000usize);
}
