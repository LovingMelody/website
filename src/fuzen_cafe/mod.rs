mod data;
//mod users;

// TODO: /login - login with discord
// TODO: /login?<TOKEN> - Show user info & signout button
// TODO: /wordgame - Store score in session cookie
// TODO: ADD Color Clock
// TODO: Implement games from https://gitlab.com/Fuzen-py/Games-rs

use crate::hosts::Hosts;

use actix_web::{web, HttpResponse, Result, Route};

use std::convert::Into;

lazy_static::lazy_static! {
    pub static ref TERA: ::tera::Tera = {
        let mut tera = ::tera::Tera::default();
        tera.add_raw_templates(vec![
            ("base", crate::statics::templates::BASE_TERA),
            ("content", crate::statics::templates::CONTENT_TERA),
            ("demos", crate::statics::templates::DEMOS_TERA),
            ("index", crate::statics::templates::INDEX_TERA),
            ("projects", crate::statics::templates::PROJECTS_TERA),
            ("login", crate::statics::templates::LOGIN_TERA)
        ])
        .expect("Failed to render templates");
        tera
    };
}

fn cafe() -> Route {
    web::get().guard(Hosts::FuzenCafe)
}

#[derive(Copy, Clone, Debug)]
pub struct ExternalResource {
    pub name: &'static str,
    pub url: &'static str,
}

pub enum FuzenCafeRoutes {
    Index,
    Demos,
    Projects,
    Static(Statics),
    //Profile,
    WordGame,
}

pub enum Statics {
    WordGame(WordGames),
    Image(Images),
    Style,
    Favicon,
}

impl From<Statics> for Route {
    fn from(val: Statics) -> Self {
        match val {
            Statics::Image(inner) => inner.into(),
            Statics::WordGame(inner) => inner.into(),
            Statics::Style => cafe().to(css),
            Statics::Favicon => cafe().to(favicon),
        }
    }
}

pub enum Images {
    FuzenInfo,
}

impl From<Images> for Route {
    fn from(val: Images) -> Self {
        match val {
            Images::FuzenInfo => cafe().to(img),
        }
    }
}

pub enum WordGames {
    MainJS,
    MainCSS,
}

impl From<WordGames> for Route {
    fn from(val: WordGames) -> Self {
        match val {
            WordGames::MainCSS => cafe().to(wordgame_css),
            WordGames::MainJS => cafe().to(wordgame_js),
        }
    }
}

impl From<FuzenCafeRoutes> for Route {
    fn from(val: FuzenCafeRoutes) -> Self {
        match val {
            FuzenCafeRoutes::Demos => cafe().to(demos),
            FuzenCafeRoutes::Index => cafe().to(index),
            //FuzenCafeRoutes::Profile => cafe().to(users::profile),
            FuzenCafeRoutes::Projects => cafe().to(projects),
            FuzenCafeRoutes::Static(inner) => inner.into(),
            FuzenCafeRoutes::WordGame => cafe().to(wordgame_main),
        }
    }
}

async fn index() -> Result<HttpResponse> {
    let mut context = ::tera::Context::default();
    context.insert("blog", &data::DEMO_BLOGS.to_vec());
    TERA.render("index", &context)
        .map(|result| HttpResponse::Ok().content_type("text/html").body(result))
        .map_err(|_| actix_web::error::ErrorNotFound("Render failed"))
}

fn wordgame_main() -> HttpResponse {
    HttpResponse::Ok()
        .content_type("text/html")
        .body(crate::statics::wordgame::INDEX_HTML)
}

fn wordgame_css() -> HttpResponse {
    HttpResponse::Ok()
        .content_type("text/css")
        .body(crate::statics::wordgame::MAIN_CSS)
}

fn wordgame_js() -> HttpResponse {
    HttpResponse::Ok()
        .content_type("text/javascript")
        .body(crate::statics::wordgame::MAIN_JS)
}

async fn demos() -> Result<HttpResponse> {
    let mut context = ::tera::Context::default();
    context.insert(
        "demos",
        &vec![data::Demo {
            name: String::from("Color Clock"),
            link: String::from("https://fuzen-py.github.io/color-clock/"),
            description: String::from("Changes color with time"),
            src: Some(String::from("https://github.com/Fuzen-py/color-clock")),
        }],
    );
    TERA.render("demos", &context)
        .map(|result| HttpResponse::Ok().content_type("text/html").body(result))
        .map_err(|_| actix_web::error::ErrorNotFound("Render failed"))
}

async fn projects() -> Result<HttpResponse> {
    let mut context = ::tera::Context::default();
    context.insert("projects", &[(); 1]);
    TERA.render("projects", &context)
        .map(|result| HttpResponse::Ok().content_type("text/html").body(result))
        .map_err(|_| actix_web::error::ErrorNotFound("Render failed"))
}

fn favicon() -> HttpResponse {
    HttpResponse::Ok()
        .content_type("image/png")
        .body(crate::statics::images::FUZEN_CAFE_FAVICON)
}

fn css() -> HttpResponse {
    HttpResponse::Ok()
        .content_type("text/css")
        .body(crate::statics::STYLE_SHEET)
}

fn img() -> HttpResponse {
    HttpResponse::Ok()
        .content_type("image/png")
        .body(crate::statics::images::FUZEN_INFO_FAVICON)
}
