pub static INDEX_HTML: &str = include_str!("index.html");
pub static MAIN_CSS: &str = include_str!("main.css");
pub static MAIN_JS: &str = include_str!("main.js");
